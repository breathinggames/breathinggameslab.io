# Game Dichosim

Projet initié à Paris les 16 et 17 mars 2019 en collaboration avec l'[hôpital Necker](http://hopital-necker.aphp.fr/), l'[hôpital Cochin](https://www.aphp.fr/contenu/hopital-cochin) et financé par le [Fonds FHF](http://www.fondsfhf.org/).

Equipe : Damien Fangous, Renaud Ori, Aminata D. Pierson, Maxime, Salome, Clarissa, Aline




## Inspirations

* Reigns

## Niveau, séquences, mode

xxx

## Gameplay

xxx

## Ennemis, obstacles

xxx

## Mécaniques

xxx

## Game flow

xxx

## Univers, esthétique, style visuel

xxx

## Histoire, objets, narratifs

xxx

## Contrôleurs

xxx

## Audio, interface

Gabarit Taille écran

Gabarit Web w: 1366 px	h: 768 px

References d'interfaces

![1e120a69bf366e054a5bc4c0dffd2a76](uploads/233f95556a620571ec40114b47181fe1/1e120a69bf366e054a5bc4c0dffd2a76.jpg)

![f3ae11050c8740e0646302e5506765e0](uploads/ae44754eeb4361a7b2cf91cfb49b5813/f3ae11050c8740e0646302e5506765e0.jpg)[3steps_app_2x.webp](uploads/008b892cfe5d9ca28deb7690212f1705/3steps_app_2x.webp)

## Evolution, bonus cinématique

xxx

## Personnages
