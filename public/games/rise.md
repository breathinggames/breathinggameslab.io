# Game Rise

Projet initié à Paris les 16 et 17 mars 2019 en collaboration avec l'[hôpital Necker](http://hopital-necker.aphp.fr/), l'[hôpital Cochin](https://www.aphp.fr/contenu/hopital-cochin) et financé par le [Fonds FHF](http://www.fondsfhf.org/).

Equipe : Yannick Gervais, Thomas Gaudy, Nicolas Wenk, Aminata D. Pierson, Maxime, Salomé, Marlène, Aline


## Inspirations

* [Thomas was alone](https://www.youtube.com/watch?v=5K4zjNtQ3y8) - systeme narratif
* [Celeste](https://www.youtube.com/watch?v=xAUun0qKlo4) - gameplay
* [New Super Mario Bros. U Deluxe](https://youtu.be/C-JxA3CGixc?t=691) - gameplay

## Document de design de jeux

Style général : 
Rise est un mélange de jeu de plates-formes en sidescroller et de jeu narratifs (textes à lire et voice over),  mettant en scène quatre personnages aux aptitudes complémentaires, (dont plusieurs sont atteint de mucoviscidose). Le jeu est jouable en solo ou à plusieurs, jusqu'à quatre joueurs.

Contexte scénaristique : 
Les personnages font un pèlerinage sur une montagne sacrée. Les motivations des personnages sont diverses. Accomplissement de soi et fierté pour certains, espoir que ce périple constitue un symbole pour d'autres. L'ascension va être de plus en plus éprouvante, à mesure que le niveau d'oxygène lié à l'altitude et aux capacités respiratoire de chacun diminuent.

Aspect narratif :
À mesure que l'ascension progressent, les personnages vont discuter entre eux de différents sujets. Les difficultés respiratoires s'accentuant au fur et à mesure du parcours, les personnages vont évoquer de plus en plus frontalement leur expérience de vie en rapport avec la mucoviscidose.


## Niveau, séquences, mode

L'ascension de la montagne se divise en niveaux de type arène, c'est à dire que chaque niveau est intégralement contenu dans l'écran, cela afin de facilité la déambulation des différents personnages dans un même espace visuel.

Pour un premier prototype, 5 niveau sont envisagés, avec des thématiques graphiques qui évoluent.

1 - ville
2 - campagne
3 - bois
4 - toundra
5 - l'arrivée au sommet

Le style sera très simples : la thématique du décor sera présenté en fond d'écran tandis que le calque où évolueront les personnages contiendra les plateformes et obstacles.

Le jeu présentent deux types de séquences complémentaires :
Des phases d'action / plateformes, susceptibles de plaire aux adolescents et jeunes adultes, pour le challenge et la dextérité requise. 

Des phases narratives, se déclenchant sous la forme de bulles de textes, de monologues ou de dialogues audio, qui se déclenchent à certains moments du jeu : 
- atteintes de certains points de progression dans le jeu
- déclenchement en lien avec une variable temporelle
- rencontre avec un npc

## Mécaniques

- déplacement horizontal
- Niveau de stamina
- Saut
- double saut
- vol plané (glide)
- élan de vitesse (dash)
- 
- Changement de niveau
- actionner des mécanismes ponctuels
- récolte d'objets dans l'arène

## Gameplay

Le ou les joueurs contrôlent leur personnage avec une manette de jeu.
Les commandent sont simples :
gauche et droite pour se diriger dans les directions correspondantes.
Un bouton dédié au saut.
Un bouton dédié à une habilité spéciale. Chacun des quatre personnages comporte une habilité différentes.
- Un double saut pour l'un (les boutons saut comme le bouton de pouvoir spécial déclenche les sauts et double sauts.)
- Un dash pour un autre (accélération brutales, à la façon de céleste).

## Ennemis, obstacles

xxx

## Game flow

xxx

## Univers, esthétique, style visuel

xxx

## Histoire, objets, narratifs

xxx

## Contrôleurs

xxx

## Audio, interface

xxx

## Evolution, bonus cinématique

xxx

## Personnages

* Fille 1
Stamina

Fille 2
Stamina

Gars 1
Stamina

## Frameworks

### Fungus

Fungus helps you bring your story to life as a game! We make it easy for you to add colourful characters and craft gripping storylines, even if you have never used Unity 3D before.

Our intuitive visual scripting lets everyone create beautiful story games - for free and with no coding! Power users can use Lua scripting to tackle bigger storytelling projects.

[Fungus Framework](http://fungusgames.com/)

### xxx

xxx

Gars 2
Stamina
