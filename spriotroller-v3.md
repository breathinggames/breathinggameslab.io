![Final](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_25_final.jpg "Final")


# A. Context

## Goals

This controller is part of the [Breathing Games](https://www.breathinggames.net/en) initiative to encourage everyone to become creators of our collective respiratory health.

Its first purpose is to transform the expiratory breath flow into an input to play games. Secondarily, it could be used to self-assess one's breathing capacity.

## Validity 
The outcomes of the controller and experience of use are being clinically tested at [Sainte-Justine hospital](https://www.chusj.org) and [Quebec hospital](https://www.chudequebec.ca). Users tested the previous version of the controller at different events held in Canada and Europe.

The reproducibility of the controller is being tested at the [Great Besançon fablab](https://fablab.grandbesancon.fr). 


## Restrictions

The recipient of the information provided here is responsible for verifying its accuracy, reliability and pertinence, if needed with the support of professionals. All data provided are for educational puropose only and do not constitute any legal or other kind of advice. Under no circumstance, including negligence can the contributors be held responsible for any loss or damage of any nature.

## Licencing

Soon

## Core contributors

Current version: Collin Gallacher (mechatronics engineer), Steven Ding (electronics engineer), Sze Man Tse (lung specialist), Myriam Bransi (paediatrician), Alena Valderrama (public health physician), Pierre-Regis Burgel (lung specialist), Yannick Gervais (game developer), Clement Marthe (game designer), Maria Frangos (user experience designer), Fabio Balli (human systems engineer).

Building on previous contributions of: John Danger (product designer), Tiberius Brastaviceanu (physics engineer), Jim Anastassiou (electronician), Povilas Jurgaitis (electronics engineer), Bernard Dugas (system architect), Helder Santos (3d designer).

A project supported by: [Canadian Institutes of Health Research](http://cihr-irsc.gc.ca), [French Hospitals Federation - Research and innovation fund](http://www.fondsfhf.org), [Concordia University](http://www.concordia.ca), [OpenCare (EU Horizon)](http://opencare.cc), [Necker hospital](http://hopital-necker.aphp.fr), [Cochin hospital](https://www.aphp.fr/contenu/hopital-cochin-3), [Sensorica](http://www.sensorica.co), [Breathing Games Association](http://www.breathinggames.net/association).


# B. Knowledge and tools required

## Competencies

Soon

##  Tools

* Computer with Internet access and USB 2 port
* 3d printer using fused deposition modeling
* Soldering iron 50W 120V

## Software

* Web browser - [Firefox](https://www.mozilla.org/en-US/firefox/new/)
* 3D printing toolbox - [Slic3r](https://slic3r.org/download/) or [Cura](https://ultimaker.com/software/ultimaker-cura)
* Arduino code editor - [Arduino IDE software](https://www.arduino.cc/en/Main/software)
* Integrated development environment - [Processing](https://processing.org/download/)


# C. Bill of materials

| Material                                              | Quantity        | CAD   | Suggested material |
| ----------------------------------------------------- | --------------- | ----- | ------------------ |
| 3d printing antibacterial filament 1.75 mm            | 1 for  N        | 62.94 | [Taulman Guidl!ne PETG](https://taulman3d.com/index.html) or [EPR InnoPET](https://www.innofil3d.com/product/epr-innopet-black-1-75mm-750gr/) |
| Lead free solder                                      | 1 for  N        | 86.88 | [Lead Free RMA](https://www.digikey.ca/products/en?keywords=82-109-ND) | 
| Electrical wire                                       | 1 for  N        | 21.88 | [24 Gauge electrical wire](https://www.amazon.ca/CBAZYTM-Stranded-Gauge-colors-Electrical/dp/B075M2XG8Z/ref=sr_1_5?keywords=24+gauge+wire&qid=1572986694&sr=8-5) | 
| Arduino processor with bluetooth                      | 1 for  1        | 42.35 | [AdaFruit Feather 32U4 BlueFruit](https://www.digikey.ca/products/en?keywords=1528-1517-ND) |
| Prototyping board                                     | 1 for  1        |  7.00 | [FeatherWing Proto - Prototyping](https://www.digikey.ca/products/en?keywords=1528-1622-ND) |
| Connector                                             | 1 for  1        |  1.34 | [Feather Header Kit FML](https://www.digikey.ca/products/en?keywords=1528-1560-ND) |
| Differential pressure sensor with amplifier           | 1 for  1        | 11.21 | [MP3V5010DP](https://www.digikey.ca/product-detail/en/nxp-usa-inc/MP3V5010DP/MP3V5010DP-ND/2186183) |
| Switch button                                         | 3 for  1        |  0.45 | [Switch tactile spst-no 0.05a 24v](https://www.digikey.ca/product-detail/en/te-connectivity-alcoswitch-switches/1825910-6/450-1650-ND/1632536) |
| Battery                                               | 1 for  1        |  5.99 | [Lithium-Ion Polymer (LiPo) Battery (3.7V 110mAh)](https://www.canadarobotix.com/products/939) |
| Wire USB-A to micro USB-B                             | 1 for  1        |  3.75 | [Cable USB-A to micro USB-B 0.3M](https://www.digikey.ca/products/en?keywords=AE11229-ND) |
| Flexible silicon tubes 4 mm / 2 mm inside             | 1 for  N (10 cm) |  7.51 | [Uxcell Silicone Tube 2mm ID x 4mm OD 9.84ft](https://www.amazon.ca/Uxcell-a15121600ux1003-Silicone-Grade-Water/dp/B01C3GCZTM/ref=sr_1_1?keywords=2mm+x+4mm+tube&qid=1572985799&s=lawn-garden&sr=1-1-catcorr)  |
| Nuts                                                  | 100 for 16 (6 each) | 11.99 | [Tapered Heat-Set Inserts for Plastic Brass, M2.5 x 0.45 mm Thread Size, 3.4 mm Installed Length](https://www.mcmaster.com/94180a321) |
| Bolts 8 mm                                            | 100 for 25 (4 each) | 5.42 | [M2.5 x 0.45 mm Thread, 8 mm Long](https://www.mcmaster.com/91292a012)  |
| Bolts 4 mm                                            | 100 for 50 (2 each) | 6.16 | [18-8 Stainless Steel Socket Head Screw M2.5 x 0.45 mm Thread, 4 mm Long](https://www.mcmaster.com/91292a015) |
| OPTION - Double sided tape                            | 1 for N (3 cm)  |       |   |
 

# D. Hardware production

## 3-d printing of enclosure

* Download the folder on https://gitlab.com/breathinggames/bg_device/blob/master/h_spirotroller_enhanced/3d_printing/spiro-enhanced_bottom_190320.stl
* Download the folder on https://gitlab.com/breathinggames/bg_device/blob/master/h_spirotroller_enhanced/3d_printing/spiro-enhanced_top_190320.stl
* Align parts on 3d printer build plate as seen in figure below. 
* Set up the printer according to the filament. The parts are meant to be printed with support material and a layer height of 0.2 mm.
  * For the Taulman Guidl!ne PETG on an Prusa MK3 printer, set up the head between 245 and 252 °C with a heated bed at 60 °C. Settings can be downloaded at https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/3d%20printing/Slic3r_config_bundle.ini
  * For the EPR InnoPET on a Ultimaker printer, set up the head between 210 and 220 °C with a heated bed at 60 °C.

![3d print](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_01_3d-print.jpg "3d print")


* Download the folder on https://gitlab.com/breathinggames/bg_device/blob/master/h_spirotroller_enhanced/3d_printing/spiro-enhanced_button1_190320.stl
* Download the folder on https://gitlab.com/breathinggames/bg_device/blob/master/h_spirotroller_enhanced/3d_printing/spiro-enhanced_button2_190320.stl
* Align parts on 3d printer build plate as seen in figure below - **button1** is printed twice. 
* Set up the printer according to the filament. The parts are meant to be printed with support material and a layer height of 0.2 mm.
  * For the Taulman Guidl!ne PETG on an Prusa MK3 printer, set up the head between 245 and 252 °C with a heated bed at 60 °C. Settings can be downloaded at https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/3d%20printing/Slic3r_config_bundle.ini
  * For the EPR InnoPET on a Ultimaker printer, set up the head between 210 and 220 °C with a heated bed at 60 °C.

![3d print](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_02_3d-print.jpg "3d print")

* Download the folder on https://gitlab.com/breathinggames/bg_device/blob/master/h_spirotroller_enhanced/3d_printing/spiro-enhanced_gain_190320.stl
* Align parts on 3d printer build plate as seen in figure below. 
* Set up the printer according to the filament. The parts are meant to be printed with support material, 15 mm brim to adhere to bed, and a layer height of 0.2 mm.
  * For the Taulman Guidl!ne PETG on an Prusa MK3 printer, set up the head between 245 and 252 °C with a heated bed at 60 °C. Settings can be downloaded at https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/3d%20printing/Slic3r_config_bundle.ini
  * For the EPR InnoPET on a Ultimaker printer, set up the head between 210 and 220 °C with a heated bed at 60 °C.

![3d print](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_03_3d-print.jpg "3d print")


## Electronics

* Cut out a small square notch from the FeatherWing Proto for easier installation of the air hose from the lower nozzle of the pressure sensor.

![Board](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_06_board.jpg "Board")
![Board](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_07_board.jpg "Board")

* Place the pressure sensor on the modified FeatherWing Proto.

![Board](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_08_board.jpg "Board")

* Solder the sensor once it is well aligned.

![Board](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_09_board.jpg "Board")

* Place the three tact buttons on the modified FeatherWing Proto. Solder the buttons once they are well aligned.

![Board](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_10_board.jpg "Board")
![Component placement top view on the FeatherWing Proto](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_04_circuit.jpg "Component placement top view on the FeatherWing Proto")

* Flip the board around to make the wire connections. Once the connections are complete, solder on the header pins to the sides of the modified FeatherWing Proto.

![Board](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_11_board.jpg "Board")
![Wire Connections bottom view of FeatherWing Proto board](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_05_circuit.jpg "Wire Connections bottom view of FeatherWing Proto board")

## Mounting

* Use a soldering iron to insert four **brass heat inserts** into the plastic housing **top**.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_12_mount.jpg "Mount")

* Insert **button2** as follow.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_13_mount.jpg "Mount")

* Insert both **button1** as follow.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_14_mount.jpg "Mount")

* Use a soldering iron to insert two heat inserts into the plastic housing **bottom**.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_15_mount.jpg "Mount")

* Mount the Adafruit Feather to the board using two **4 mm screws**.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_16_mount.jpg "Mount")

* Connect the **battery** to the board and align it as follows. You can use double sided tape to fix it to the board.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_17_mount.jpg "Mount")

* Cut the **Silicone tubing** to 30 mm and 40 mm lengths and connect them to the breathing tube as follows. Below a piece of double sided tape is added to the end of the device to keep it fixed properly (to be modified in future design).

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_18_mount.jpg "Mount")

* Connect the tubes to the board built above.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_19_mount.jpg "Mount")

* Mount the board to the **Adafruit Feather 32u4 Bluefruit LE** as follows.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_20_mount.jpg "Mount")

* Align the **top** to be closed around the **bottom**.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_21_mount.jpg "Mount")

* Close and orient the device facing downwards.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_22_mount.jpg "Mount")

* Use four **8 mm screws** and screw them into the available slots.

![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_23_mount.jpg "Mount")
![Mount](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_24_mount.jpg "Mount")

* The device is now ready to be plugged in and programmed by the computer.

![Final](https://gitlab.com/breathinggames/bg_device/raw/master/h_spirotroller_enhanced/steps/spiro-enhanced_25_final.jpg "Final")


# E. Software production

## Setting Arduino IDE software on your computer

Tested with version 1.8.19.0.
1. Install the latest version of [Arduino IDE](https://www.arduino.cc/en/Main/Software)
2. Start Arduino IDE, go to the **File** menu in Windows or Linux, or the **Arduino** menu on Mac OS, and click **Preferences**.
3. In the text field **Additional Boards Manager URLs**, copy this text: 
```
https://adafruit.github.io/arduino-board-index/package_adafruit_index.json
```
Click **OK**.
4. Go to the **Sketch menu**, click **Include Library** and **Manage Libraries…**. In the search box enter 
```
Adafruit nRF51 Bluefruit
```
Click on the result and click **Install**.
5. In the search box enter 
```
MegunoLink
```
Click on the result and click **Install**.
6. Go to the **Tools** menu, to the **Board** sub-menu, click **Boards Manager**. In the search box, enter 
```
Adafruit AVR Boards or 32u4
```
Click **Install**.

7. Quit Arduino IDE.
8. Start Arduino IDE, go to the **Tools** menu, to the **Board** sub-menu, and select **Adafruit Feather 32u4**.
10. Plug the game controller on the computer USB port.
11. Go to the **Tools** menu, to the **Port** sub-menu, and select the option with **(Adafruit Feather 32u4)**.
12. Windows only: if the option does not appear, install the driver from https://github.com/adafruit/Adafruit_Windows_Drivers/releases/download/2.3.4/adafruit_drivers_2.3.4.0.exe. Once done, do 8, 11 and continue from 13.
13. Download the script https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/arduino%20software/SpiroMeter_Serial/SpiroMeter_Serial.ino
14. Go to the **File** menu, click **Open**, select the file **.ino** just downloaded and click **Open**.
15. Go to the **Sketch** menu and click on **Update**.
16. Go to the **Sketch** menu, and click **Verify/Compile**.


## Adding bluetooth functionnality

Soon

# F. Testing

## Setting the Processing software on your computer

1. Install the latest version of [Processing](https://processing.org/download/)
2. Download the script from https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/processing%20demo/FlowTestSpiro/FlowTestSpiro.pde 
3. Start Processing, go to the **File** menu, click **Open**, select the file **.pde** just downloaded and click **Open**.
4. Go to the **Sketch** menu, and click **Run**.


# G. Use

Soon