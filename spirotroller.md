![Spirotroller](https://gitlab.com/breathinggames/bg_device/raw/master/-%20Design/g_spirotroller.png "Spirotroller")

[[_TOC_]]

# Knowledge and material required

## Competencies

TBD

##  Tools

* Computer with Internet access and USB 2 port
* 3d printer using fused deposition modeling
* Soldering iron 50W 120V

## Software

* Web browser - [Firefox](https://www.mozilla.org/en-US/firefox/new/)
* 3D printing toolbox - [Slic3r](https://slic3r.org/download/) or [Cura](https://ultimaker.com/software/ultimaker-cura)
* Arduino code editor - [Arduino IDE software](https://www.arduino.cc/en/Main/software)
* Integrated development environment - [Processing](https://processing.org/download/)


# Material

| Material                                              | Quantity        | CAD   | Suggested material |
| ----------------------------------------------------- | --------------- | ----- | ------------------ |
| 3d printing antibacterial filament 1.75 mm            | 1 for  N        | 62.94 | [Taulman Guidl!ne PETG](https://taulman3d.com/index.html) or [EPR InnoPET](https://www.innofil3d.com/product/epr-innopet-black-1-75mm-750gr/) |
| Lead free solder                                      | 1 for  N        | 86.88 | [Lead Free RMA](https://www.digikey.ca/products/en?keywords=82-109-ND) | 
| Arduino processor with bluetooth                      | 1 for  1        | 42.35 | [AdaFruit Feather 32U4 BlueFruit](https://www.digikey.ca/products/en?keywords=1528-1517-ND) |
| Prototyping board                                     | 1 for  1        |  7.00 | [FeatherWing Proto - Prototyping](https://www.digikey.ca/products/en?keywords=1528-1622-ND) |
| Connector                                             | 1 for  1        |  1.34 | [Feather Header Kit FML](https://www.digikey.ca/products/en?keywords=1528-1560-ND) |
| Differential pressure sensor 1.45 PSI  with amplifier | 1 for  1        | 26.76 | [Sensor pressure integrated silic](https://www.digikey.ca/products/en?keywords=MPXV5010DP-ND) |
| 120 Ohms resistor                                     | 1 for  1        |  0.15 | [Res 120 ohm 1/4w 5% axial](https://www.digikey.ca/products/en?keywords=CF14JT120RCT-ND) |
| Light-emitting diode                                  | 1 for  1        |  0.38 | [LED green clear t-1 3/4 t/h](https://www.digikey.ca/products/en?keywords=160-1131-ND) |
| Switch button                                         | 1 for  1        |  0.15 | [Switch tactile spst-no 0.05a 24v](https://www.digikey.ca/products/en?keywords=450-1650-ND) |
| Wire USB-A to micro USB-B                             | 1 for  1        |  3.75 | [Cable USB-A to micro USB-B 0.3M](https://www.digikey.ca/products/en?keywords=AE11229-ND) |
| Flexible silicon tubes 4 mm / 2 mm inside             | 1 for  1, 10 cm |       |  |
| Screws                                                | 4 for  1        |       |  |


# Step by step reproduction

## 3-d printing of enclosure

* Download the folder on https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/3d%20printing/Spirometer%20v3.step
* Set up the printer according to the filament. The parts are meant to be printed with no support material.
  * For the Taulman Guidl!ne PETG on an Prusa MK3 printer, set up the head at 250 °C with a heated bed at 60 °C. Settings can be downloaded at can be downloaded at https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/3d%20printing/Slic3r_config_bundle.ini
  * For the EPR InnoPET on a Ultimaker printer, set up the head at 220 °C with a heated bed at 60 °C.

## Hardware

![Electronics layout](https://gitlab.com/breathinggames/bg_device/raw/master/g_spirotroller/electronic_layout.png "Electronics layout")

* Attach the pressure sensor to the prototyping board using the following pinout configuration
  * SensorPin2 to 5V
  * SensorPin3 to Ground
  * SensorPin4 to A0
* Connect the switch button to the prototypong board
  * GND to Pin12
* Connect the LED to the prototypong board
  * Pin11 to GND using a 500 Ohm resistor

![Mounting](https://gitlab.com/breathinggames/bg_device/raw/master/-%20Design/g_spirotroller_photos.jpg "Mounting")

* Connect the prototyping board to the Arduino board using the connector


## Software

### Setting Arduino IDE software on your computer

Tested with version 1.8.19.0.
1. Install the latest version of [Arduino IDE](https://www.arduino.cc/en/Main/Software)
2. Start Arduino IDE, go to the **File** menu in Windows or Linux, or the **Arduino** menu on Mac OS, and click **Preferences**.
3. In the text field **Additional Boards Manager URLs**, copy this text: 
```
https://adafruit.github.io/arduino-board-index/package_adafruit_index.json
```
Click **OK**.
4. Go to the **Sketch menu**, click **Include Library** and **Manage Libraries…**. In the search box enter 
```
Adafruit nRF51 Bluefruit
```
Click on the result and click **Install**.
5. In the search box enter 
```
MegunoLink
```
Click on the result and click **Install**.
6. Go to the **Tools** menu, to the **Board** sub-menu, click **Boards Manager**. In the search box, enter 
```
Adafruit AVR Boards or 32u4
```
Click **Install**.

7. Quit Arduino IDE.
8. Start Arduino IDE, go to the **Tools** menu, to the **Board** sub-menu, and select **Adafruit Feather 32u4**.
10. Plug the game controller on the computer USB port.
11. Go to the **Tools** menu, to the **Port** sub-menu, and select the option with **(Adafruit Feather 32u4)**.
12. Windows only: if the option does not appear, install the driver from https://github.com/adafruit/Adafruit_Windows_Drivers/releases/download/2.3.4/adafruit_drivers_2.3.4.0.exe. Once done, do 8, 11 and continue from 13.
13. Download the script https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/arduino%20software/SpiroMeter_Serial/SpiroMeter_Serial.ino
14. Go to the **File** menu, click **Open**, select the file **.ino** just downloaded and click **Open**.
15. Go to the **Sketch** menu and click on **Update**.
16. Go to the **Sketch** menu, and click **Verify/Compile**.


### Adding bluetooth functionnality



# Testing the controller

### Setting the Processing software on your computer

1. Install the latest version of [Processing](https://processing.org/download/)
2. Download the script from https://gitlab.com/breathinggames/bg_device/blob/master/g_spirotroller/processing%20demo/FlowTestSpiro/FlowTestSpiro.pde 
3. Start Processing, go to the **File** menu, click **Open**, select the file **.pde** just downloaded and click **Open**.
4. Go to the **Sketch** menu, and click **Run**.



# Calibration

## Calibration siringhe

# Use

## Audience and use
## Restrictions
## Steps

# Improvements    

